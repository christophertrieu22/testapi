﻿using System;
using System.Collections.Generic;
using yortrip.Entities;

namespace yortrip.Services
{
    public interface INotificationRepository
    {
        IEnumerable<Notification> GetNotifications(Guid calendarId);

        Notification GetNotification(Guid notificationId);

        void AddNotification(Guid calendarId, Guid userId);

        void UpdateNotification(Notification notification);

        void DeleteNotification(Notification notification);

        bool SaveChanges();
    }
}
