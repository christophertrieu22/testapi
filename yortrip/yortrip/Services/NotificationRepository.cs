﻿using System;
using System.Collections.Generic;
using System.Linq;
using yortrip.DbContexts;
using yortrip.Entities;


namespace yortrip.Services
{
    public class NotificationRepository : INotificationRepository, IDisposable
    {
        private readonly CalenderContext _context;

        private UserRepository userRepositroy;

        public NotificationRepository(CalenderContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }


        public void DeleteNotification(Notification notification)
        {
            if (notification == null)
            {
                throw new ArgumentNullException(nameof(notification));
            }

            _context.Notifications.Remove(notification);


        }

        public Notification GetNotification(Guid notificationId)
        {
            if (notificationId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(notificationId));
            }

            return _context.Notifications.Where(n => n.NotificationId == notificationId && n.Viewed == false).FirstOrDefault();
        }

        public IEnumerable<Notification> GetNotifications(Guid calendarId)
        {
            if (calendarId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(calendarId));
            }

            List<Notification> notifications = _context.Notifications.Where(n => n.UserId == calendarId && n.Viewed == false).ToList();

            return notifications;
        }

        public void AddNotification(Guid calendarId, Guid userId)
        {
            if (calendarId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(calendarId));
            }

            if (userId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            Notification notification = new Notification
            {
                CalendarId = calendarId,

                UserId = userId,

                Message = "UserName has added their dates to CalendarName",

                Viewed = false,

                CreatedDate = DateTime.Now
            };

            _context.Notifications.Add(notification);
        }

        public void UpdateNotification(Notification notification)
        {
            // no code in this implementation
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose resources when needed
            }
        }

    }
}
